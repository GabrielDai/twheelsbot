# twheelsbot

ROS2 package for controlling [3Wheelsbot](https://gitlab.com/GabrielDai/3Wheelsbot).

## Topics

* **/motion_cmd**: listening to this topic for new motion commands
* **/motors_vin**: here are published the voltage input of the motors
* **/motors_rev**: here are published the revolution of the motors
* **/heading**: here is published the heading (yaw) of the bot

**Note:** In order to activate messages in **/motors_vin** and **/motors_rev**
you need to activate the logging pressing the keys **v** and/or **r** respectively.

## Building with colcon

You will need this [package](https://gitlab.com/GabrielDai/diff-drive-msg), that
contains the message types.

Don't forget to source your working directory.

``` shell
colcon build --symlink-install --packages-select twheelsbot
```

## Run

``` shell
ros2 run twheelsbot app <IPv4 of your bot>
```
