from setuptools import setup

package_name = 'twheelsbot'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Gabriel Moyano',
    maintainer_email='vgmoyano@gmail.com',
    description='Package for controlling the 3Wheelsbot',
    license='GNU Lesser General Public License version 3',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'app = twheelsbot.app:main',
        ],
    },
)
