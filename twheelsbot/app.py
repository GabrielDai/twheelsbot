#!python3
#
# SPDX-FileCopyrightText: © 2021 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import argparse
import sys
import rclpy
from rclpy.node import Node
from .robotControl import app as robotControl
from .robotControl import comm
from diff_drive_msg.msg import Heading
from diff_drive_msg.msg import MotorsVin
from diff_drive_msg.msg import MotorsRev
from geometry_msgs.msg import Twist


class TWheelsBotBridge(Node):

    def __init__(self):
        super().__init__('TWheelsBot_bridge')
        self.motion_cmd_sub = self.create_subscription(Twist, 'motion_cmd', self.listener_callback, 10)
        self.motor_vin_pub = self.create_publisher(MotorsVin, 'motors_vin', 100)
        self.motor_rev_pub = self.create_publisher(MotorsRev, 'motors_rev', 100)
        self.heading_pub = self.create_publisher(Heading, 'heading', 10)

        timer_period = 0.1  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

        self.motion_cmds = []
        self.motors_vin = []
        self.motors_rev = []
        self.current_yaw = 0.0

    def listener_callback(self, msg):
        vX = msg.linear.x
        vY = msg.linear.y
        vO = msg.angular.z
        dt = 1 # 1 second
        cmd = comm.MotionCmd(vX, vY, vO, dt)
        self.motion_cmds.append(cmd)

    def timer_callback(self):
        heading_msg = Heading()
        heading_msg.yaw = self.current_yaw
        self.heading_pub.publish(heading_msg)

        motors_vin_msg = MotorsVin()
        if self.motors_vin:
            while self.motors_vin:
                motors_vin = self.motors_vin.pop(0)
                motors_vin_msg.left = motors_vin.left_wheel
                motors_vin_msg.right = motors_vin.right_wheel
                self.motor_vin_pub.publish(motors_vin_msg)
        else:
            motors_vin_msg.left = 0.0
            motors_vin_msg.right = 0.0
            self.motor_vin_pub.publish(motors_vin_msg)

        # TODO refactoring this, it's almost the same code
        motors_rev_msg = MotorsRev()
        if self.motors_rev:
            while self.motors_rev:
                motors_rev = self.motors_rev.pop(0)
                motors_rev_msg.left = motors_rev.left_wheel
                motors_rev_msg.right = motors_rev.right_wheel
                self.motor_rev_pub.publish(motors_rev_msg)
        else:
            motors_rev_msg.left = 0.0
            motors_rev_msg.right = 0.0
            self.motor_rev_pub.publish(motors_rev_msg)


def main(args=None):
    # Parsing arguments
    args_parser = argparse.ArgumentParser(description='Simple 3WheelsBot remote controlling app compatible with ROS2.')
    args_parser.add_argument("ip_addr", help="ip address of the robot")
    args_parser.add_argument("-p", "--port",type=int, nargs="?", default=65000, help="TCP port")
    arguments = args_parser.parse_args(sys.argv[1:])

    rclpy.init(args=args)
    ros_bridge = TWheelsBotBridge()

    with robotControl.App(arguments.ip_addr, arguments.port, ros_bridge) as robot_control:
        while robot_control.running:
            robot_control.execute_tasks()
            rclpy.spin_once(ros_bridge)

    ros_bridge.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
